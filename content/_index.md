+++
title = "index"
#framed = "true"
+++

# Who I am
I was born and raised in Boulder, Colorado and currently attend the University of Minnesota in Minneapolis. I study Materials Science and Engineering with minors in Chemistry and Political Science. My primary research interests are in sustainable polymers--specifically degradation and mechanical properties.
# Experience
My relevant experience can be found in my resume linked at the top of the page. This website presents samples of my writing and data analysis.

---
---