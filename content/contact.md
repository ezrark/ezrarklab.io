+++
title = "Contact"
+++
# How To Reach Me
### Reliable methods
If you want to hear back from me in a reasonable amount of time:
* Email: [ezra.kone@gmail.com](mailto:ezra.kone@gmail.com)
* Phone/Whatsapp/Signal: [720-639-1776](tel:7206391776)

---

### Mailing Address
If you wish to support our brave workers in blue by utilizing the United States Postal Service, you may mail me at:
> 310 15th Ave SE
>
> Apt 401
>
> Minneapolis, MN 55414