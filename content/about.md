+++
title = "About"
+++
# Who I Am
I was born and raised in Boulder, CO and now attend the University of Minnesota in Minneapolis. I study Materials Science and Engineering with minors in Chemistry and Political Science. My primary research interests are in sustainable polymers--specifically degradation and mechanical properties.
# Experience
**A concise summary of my experience can be found in my resume linked at the top of the page.**

I have experience in chemical mixing and preparation from my general chemistry lab courses. My material analysis experience includes [x-ray diffraction](/posts/xrd), scanning electron microscopy, and atomic force microscopy.