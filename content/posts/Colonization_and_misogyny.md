+++
title = "Essay on Colonization and Misogyny"
subtitle = "Written for POL 3265: Ideas and Protest in French Postwar Thought"
date = 2020-05-13
+++
The attached essay was written as the final assignment for Political Science 3265: Ideas and Protest in French Postwar Thought. The course was taught by Professor Nancy Luxon at the University of Minnesota.

[Economic Differences of Colonization and Misogyny](/Documents/3265_final.pdf)