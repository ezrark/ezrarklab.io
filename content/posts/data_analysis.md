+++
title = "Data Analysis and Presentation"
date = 2020-12-09
+++
When I first started my [X-Ray Diffraction Lab Report](/posts/xrd), I attempted to conduct my data analysis and presentation exclusively using the [Julia](https://julialang.org) scientific computing language. I had never used Julia before and my previous experience with scientific programming was in Matlab, R, and Mathematica. However, my previous experience proved useful and I proceeded quickly. Later on, however, as the math and data structures became more complex, I realized that using Julia was not efficient given my inexperience with the language and the timeframe on which I was working.

The following includes examples of analysis I performed and figures I generated using Julia. I am excited to gain more experience using this wonderfully promising scientific computing language. The full source of my Julia code can be found [on Gitlab](https://gitlab.com/ezrark/3801-lab-report/-/blob/master/Julia/main.jl).

## XRD Pattern Plotting:
I performed all of the plotting using the Gadfly packages, which uses the standard Grammar of Graphics also used by ggplot2 for R. The below image is a plot of the raw data collected from a Rigaku MiniFlex 600 x-ray diffractometer. I've annotated the plot with the miller indices of the planes corresponding with the pattern peaks.

{{< figure src="/images/Cu_Foil.PNG" title="Copper foil XRD pattern produced using raw data plotted in Julia using Gadfly" >}}
The code for the plot:
{{< highlight julia >}}
Cu_plot = Gadfly.plot(x=CuFoil[:,1], y=CuFoil[:,2], Geom.line, Coord.cartesian(xmin=30), Guide.xticks(ticks=[30:10:130;]), Guide.yticks(ticks=[0:5000:2.1e4;]), Guide.xlabel("2θ [°]"), Guide.ylabel("Intensity [cts]"), Guide.manual_color_key("", ["Cu Foil"], [colorant"blue"], pos=[0.76w,-.43h]), Guide.annotation(compose(context(), Compose.text(peaks.Cu_angles, [1.9e4,1.35e4,7e3,5e3,2e3,2e3], ["(111)","(200)","(220)","(311)","(222)","(400)"]), Compose.font("Helvetica"))))
{{< /highlight >}}

## Lattice Parameter Calculations:
{{< highlight julia >}}
# sample represents a column in the peaks dataframe, number is the plane number of the sample for which we want the lattice parameter
param(sample,number) = (lambda * sqrt(sum(.^(planes[number,:], 2)))) / (2 * sind(peaks[number,sample] / 2))
Cu_data = DataFrame(peak_angles=[43.3256,50.4071,74.105,89.8716,95.1253,116.7618], params=param.(1, [1:6;]))
Cu63Ni37_data = DataFrame(peak_angles=[43.6629, 50.8727, 74.8569, 90.9075, 96.2104, 118.5825], params=param.(2, [1:6;]))
Cu26Ni74_data = DataFrame(peak_angles=[44.0274, 51.3403, 75.6535, 92.0137, 97.4548], params=param.(3, [1:5;]))
Ni_data = DataFrame(peak_angles=[44.3753, 51.7256, 76.2595, 92.7877, 98.3305, 121.7872], params=param.(4, [1:6;]))
unknown_data = DataFrame(peak_angles=[43.895, 51.1754, 75.3833, 91.6447, 97.0304, 119.904], params=param.(5, [1:6;]))
{{< /highlight >}}

The following code block requires a more in-depth knowledge of what I was working to accomplish. If you truly wish to understand it, I recommend you read [the report](/Documents/stage3.pdf).

{{< highlight julia >}}
# calculate the x value for each lattice parameter and put it in the respective column of the dataframe
Cu_data.xvals = (.^(cosd.(peaks.Cu_angles ./ 2), 2) ./ ((sind.(peaks.Cu_angles ./ 2))))
Cu63Ni37_data.xvals = (.^(cosd.(peaks.C6N3_angles ./ 2), 2) ./ ((sind.(peaks.C6N3_angles ./ 2))))
Cu26Ni74_data.xvals = (.^(cosd.(Cu26Ni74_data.peak_angles ./ 2), 2) ./ ((sind.(Cu26Ni74_data.peak_angles ./ 2))))
Ni_data.xvals = (.^(cosd.(Ni_data.peak_angles ./ 2), 2) ./ ((sind.(Ni_data.peak_angles ./ 2))))
unknown_data.xvals = (.^(cosd.(unknown_data.peak_angles ./ 2), 2) ./ ((sind.(unknown_data.peak_angles ./ 2))))

# regressions for lattice parameters
Cu_reg = lm(@formula(params ~ xvals), Cu_data)
Cu63Ni37_reg = lm(@formula(params ~ xvals), Cu63Ni37_data)
Cu26Ni74_reg = lm(@formula(params ~ xvals), Cu26Ni74_data)
Ni_reg = lm(@formula(params ~ xvals), Ni_data)
unknown_reg = lm(@formula(params ~ xvals), unknown_data)

Cu_param_func(x) = x * coef(Cu_reg)[2] + coef(Cu_reg)[1]
Cu63Ni37_param_func(x) = x * coef(Cu63Ni37_reg)[2] + coef(Cu63Ni37_reg)[1]
Cu26Ni74_param_func(x) = x * coef(Cu26Ni74_reg)[2] + coef(Cu26Ni74_reg)[1]
Ni_param_func(x) = x * coef(Ni_reg)[2] + coef(Ni_reg)[1]
unknown_param_func(x) = x * coef(unknown_reg)[2] + coef(unknown_reg)[1]

# make a dataframe with parameters and their respective x value of Cu(1-x)Nix
param_frame = DataFrame(x=[0, .37, .74, 1], params=[coef(Cu_reg)[1], coef(Cu63Ni37_reg)[1], coef(Cu26Ni74_reg)[1], coef(Ni_reg)[1]])
vegard_reg = lm(@formula(params ~ x), param_frame)
vegard_func(x) = x * coef(vegard_reg)[2] + coef(vegard_reg)[1]

# calculate the mol% Ni of the unknown
unknown_param_find(x) = x * coef(vegard_reg)[2] + coef(vegard_reg)[1] - coef(unknown_reg)[1]
unknown_Ni_mol = find_zero(unknown_param_find, .5)
{{< /highlight >}}