+++
title = "Scanning Electron Microscopy"
date = 2020-11-25
+++

[**PDF**](/Documents/SEM.pdf)

# Overview
This report was written as answers to questions presented in the lab manual and is not written as a formal lab report. However, the writing itself is indicative of my technical writing and should be interpreted as such. The data analysis for this report was performed using the Fiji software to analyze grain size of Ag nanocrystals.

# Data Presentation
The data used in this report is presented as images from the Scanning Electron Microscopy (SEM) data collection software. The highlight of this report and reason it's presented here is the image presentation in figures with subfigure labeling and numbering. This was done in LaTeX, and I've included the source and figures below as examples.
## Example 1:
This example shows a figure with four subfigures. Each subfigure is the same size and has its own caption. The figure also has its own caption to describe the overall data being shown.
### Figure:
![Four-panel figure](/images/SEM_1-2.png)
### Source:
```latex
\begin{figure}[h]
            \begin{subfigure}{0.5\textwidth}
                \centering
                \includegraphics[width=\linewidth]{FIJI_PNGs/from_conversion/Image03_AgNanocrystals_SEI_20kV_WD10mm_SS00_25kx.png}
                \caption{Spot size SS0}
                \label{subfig:Ag_SS0}
            \end{subfigure}\hspace{2em}
            \begin{subfigure}{0.5\textwidth}
                \centering
                \includegraphics[width=\linewidth]{FIJI_PNGs/from_conversion/Image04_AgNanocrystals_SEI_20kV_WD10mm_SS15_25kx.png}
                \caption{Spot size SS15}
                \label{subfig:Ag_SS15}
            \end{subfigure}\vspace{1.5em}
            \begin{subfigure}{0.5\textwidth}
                \centering
                \includegraphics[width=\linewidth]{FIJI_PNGs/from_conversion/Image02_AgNanocrystals_SEI_20kV_WD10mm_SS30_25kx.png}
                \caption{Spot size SS30}
                \label{subfig:Ag_SS30}
            \end{subfigure}\hspace{2em}
            \begin{subfigure}{0.5\textwidth}
                \centering
                \includegraphics[width=\linewidth]{FIJI_PNGs/from_conversion/Image05_AgNanocrystals_SEI_20kV_WD10mm_SS60_25kx.png}
                \caption{Spot size SS60}
                \label{subfig:Ag_SS60}
            \end{subfigure}
            \caption{Secondary electron image micrographs of Ag nanocrystals on a Si substrate at 25,000x magnification and 20kV accelerating voltage. All images at the same scale. Spot size differs between images.}
\end{figure}
```
## Example 2:
The other figure I want to highlight is a side-by side of two images of different sizes--each with a caption--and an overall figure caption.
### Figure:
![Mixed-sizing image figure](/images/SEM_A-1.png)
### Source:
```latex
\begin{figure}[H]
        \begin{subfigure}[l]{0.6\textwidth}\label{subfig:red_threshold}
            \centering
            \includegraphics[width=\linewidth]{screenshots/red_thresholding_process.png}
            \caption{The FIJI display during the threshold-setting process. Red is used to assist the user in setting the correct threshold.}
        \end{subfigure}\hspace{2em}
        \begin{subfigure}[r]{0.3\textwidth}\label{subfig:thresholds}
            \centering
            \includegraphics[width=\linewidth]{screenshots/image02_thresholds.png}
            \caption{The threshold-setting dialog in FIJI. Sliders indicate the upper and lower grayscale values that correspond to areas in red in the image.}
        \end{subfigure}
        \caption{Screenshots of the particle threshold-setting process in the FIJI image analysis software.}
        \label{fig:threshold}
\end{figure}
```